<?php
    date_default_timezone_set("America/Buenos_Aires");
    echo date("d/m/Y H:i:s");

    echo "<hr>";

    $date = date("Y-m-d"); // DATE
    $datetime = date("Y-m-d H:i:s"); // DATETIME

    $time = time();
    echo date("d/m/Y", $time);

    echo "<hr>";

    $data_pagamento = mktime(23, 59, 59, 02, 28, 2022);
    echo date('d/m - H:i', $data_pagamento);

    echo "<hr>";

    $data = "2019-04-10 13:30:00";
    $data = strtotime($data);
    echo date("d/m/Y", $data);
?>