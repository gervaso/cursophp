<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curso de PHP</title>
</head>
<body>

<?php
    $senha = "123dO4";
    $senha_db = '$2y$10$Q4KQJFigXcrcMQDVzp9Hre3YSN.1gte459doRC4VLNHo5u5mT1Ncu';

    $novasenha = base64_encode($senha);
    echo "Base64: $novasenha<br>";
    echo "Sua senha é: " . base64_decode($novasenha);

    echo "<hr>";

    echo "MD5: " . md5($senha) . "<br>";
    echo "Sha1: " . sha1($senha) . "<br>";

    echo "<hr>";

    $options = ["cost" => 10];
    $senhaSegura = password_hash($senha, PASSWORD_DEFAULT, $options);
    echo $senhaSegura . "<br>";

    if (password_verify($senha, $senha_db)) {
        echo "Senha válida";
    } else {
        echo "Senha inválida";
    }
?>

<hr><a href="https://crackstation.net/" target="_blank">Verificar segurança da senha</a>

</body>
</html>

