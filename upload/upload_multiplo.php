<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curso de PHP</title>
</head>
<body>
    
<?php
    if (isset($_POST["enviar-formulario"])) {
        $formatosPermitidos = array("png", "jpeg", "jpg", "gif");
        //var_dump($_FILES);

        $quantidadeArquivos = count($_FILES["arquivo"]["name"]);
        $contador = 0;

        while ($contador < $quantidadeArquivos) {
            $extensao = pathinfo($_FILES["arquivo"]["name"][$contador], PATHINFO_EXTENSION);
            //echo $extensao;
            if (in_array($extensao, $formatosPermitidos)) {
                $temporario = $_FILES["arquivo"]["tmp_name"][$contador];
                $pasta = "arquivos/";
                $novoNome = uniqid() . ".$extensao";
                if (move_uploaded_file($temporario, $pasta . $novoNome)) {
                    echo "Upload feito com sucesso para $pasta$novoNome<br>";
                } else {
                    echo "Erro ao enviar o arquivo $temporario<br>";
                }
            } else {
                echo "Formato $extensao inválido<br>";
            }
            $contador++;
        }
    }
?>

<form action="upload_multiplo.php" method="POST" enctype="multipart/form-data">
    <input type="file" name="arquivo[]" multiple><br>
    <input type="submit" name="enviar-formulario">
</form>

</body>
</html>