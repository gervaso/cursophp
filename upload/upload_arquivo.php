<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curso de PHP</title>
</head>
<body>
    
<?php
    if (isset($_POST["enviar-formulario"])) {
        $formatosPermitidos = array("png", "jpeg", "jpg", "gif");
        //var_dump($_FILES);
        $extensao = pathinfo($_FILES["arquivo"]["name"], PATHINFO_EXTENSION);
        //echo $extensao;
        if (in_array($extensao, $formatosPermitidos)) {
            $temporario = $_FILES["arquivo"]["tmp_name"];
            $pasta = "arquivos/";
            $novoNome = uniqid() . ".$extensao";
            if (move_uploaded_file($temporario, $pasta . $novoNome)) {
                $mensagem = "Upload feito com sucesso!";
            } else {
                $mensagem = "Não foi possível fazer o upload";
            }
        } else {
            $mensagem = "Formato inválido";
        }
    echo $mensagem;
    }
?>

<form action="upload_arquivo.php" method="POST" enctype="multipart/form-data">
    <input type="file" name="arquivo"><br>
    <input type="submit" name="enviar-formulario">
</form>

</body>
</html>