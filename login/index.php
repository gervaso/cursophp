<!DOCTYPE html>

<?php
    // Conexão
    require_once "db_connect.php";

    // Sessão
    session_start();

    // Botão Enviar
    if (isset($_POST["btn-entrar"])) {
        $erros = array();
        $login = mysqli_escape_string($connect, $_POST["login"]);
        $senha = md5(mysqli_escape_string($connect, $_POST["senha"]));

        if (empty($login) or empty($senha)) {
            $erros[] = "<li>Os campos Login e Senha devem ser preenchidos</li>";
        } else {
            $sql = "select login from usuarios where login = '$login'";
            $resultado = mysqli_query($connect, $sql);

            if (mysqli_num_rows($resultado) > 0) {

                $sql = "select * from usuarios where login = '$login' and senha = '$senha'";
                $resultado = mysqli_query($connect, $sql);

                if (mysqli_num_rows($resultado) == 1) {
                    $dados = mysqli_fetch_array($resultado);
                    mysqli_close($connect);
                    $_SESSION["logado"] = true;
                    $_SESSION["id_usuario"] = $dados["id"];
                    header("Location: home.php");
                } else {
                    $erros[] = "<li>Usuário e senha não conferem</li>";
                }

            } else {
                $erros[] = "<li>Usuário não existe</li>";
            }
        }
    }
?>

<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Curso de PHP</title>
</head>
<body>

<h1>Login</h1>

<?php
    if (!empty($erros)) {
        foreach ($erros as $erro) {
            echo $erro . "<br>";
        }
    }
?>

<hr>

<form action="<?php echo $_SERVER['PHP_SELF'] ?>" method="POST">
    Login: <input type="text" name="login"><br>
    Senha: <input type="password" name="senha"><br>
    <button type="submit" name="btn-entrar">Entrar</button>
</form>

</body>
</html>